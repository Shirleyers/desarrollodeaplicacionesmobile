import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { Color, View } from "tns-core-modules/ui/core/view/view";
import {NoticiasService} from "../domain/noticias.service";
import * as Toast from "nativescript-toasts";

@Component({
    selector: "Search",
    moduleId: module.id,
    templateUrl: "./search.component.html",
   // providers:[NoticiasService]

})
export class SearchComponent implements OnInit {
 
    resultados:Array<string>;
    @ViewChild ("layout", {static:true}) layout:ElementRef;
   

    constructor(private noticias:NoticiasService) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.
      /*  this.noticias.agregar("hola!");
        this.noticias.agregar("hola 2!");
        this.noticias.agregar("hola 3!");*/
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
    onItemTap(x):void{
        console.dir(x);
    }
    
    buscarAhora(s: string){
        console.dir("buscarAhora" +s);
        this.noticias.buscar(s).then((r:any) =>{
            console.log("resultados buscarAhora:" +JSON.stringify(r));
            this.resultados=r;
        }, (e)=>{
            console.log("error buscarAhora" +e);
            Toast.show({text:"Error en la busqueda", duration: Toast.DURATION.SHORT});

    });
    }
}
